using Xunit;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AggregateSortingWithEntityFramework.Tests
{
    public class DatabaseFixture
    {
        private const string DatabaseName = "AggregateSortingWithEntityFramework";
        public DatabaseFixture()
        {
            using var context = CreateDatabaseAndApplyMigrations();
            SetupSeedData(context);
        }

        public static SqlConnectionStringBuilder ConnectionString 
            => new SqlConnectionStringBuilder($"Data Source=(LocalDb)\\MSSQLLocalDB;Initial Catalog={DatabaseName};Trusted_Connection=True;");

        private TestContext CreateDatabaseAndApplyMigrations()
        {
            var masterConnection = new SqlConnectionStringBuilder(ConnectionString.ConnectionString);
            masterConnection.InitialCatalog = "master";
            using var sqlConnection = new SqlConnection(masterConnection.ConnectionString);
            sqlConnection.Open();
            var command = sqlConnection.CreateCommand();
            command.CommandText = $@"IF NOT EXISTS(SELECT * FROM sys.databases WHERE name = '{DatabaseName}')
                                    BEGIN
                                        CREATE DATABASE {DatabaseName}
                                    END";
            command.ExecuteNonQuery();
            sqlConnection.Close();

            // this ctor runs migrations
            var context = new TestContext(ConnectionString.ConnectionString);
            return context;
        }

        private void SetupSeedData(TestContext context)
        {
            if (context.DataFiles.Any())
            {
                // already set up - can just exit
                return;
            }
            var random = new Random();
            string NewName(int index) => $"File_{index}.csv";
            DateTime NewCreateDate() => DateTime.Now.AddHours(-(random.Next(0, 192)));
            foreach(var index in Enumerable.Range(1, 25))
            {
                switch (index)
                {
                    case { } when index % 5 == 0:
                        // empty files (no transactions)
                        context.DataFiles.Add(new DataFile { FileName = NewName(index), CreatedDate = NewCreateDate() });
                        break;
                    default:
                        // files with some transactions
                        var file = new DataFile { FileName = NewName(index), CreatedDate = NewCreateDate() };
                        var transactions = Enumerable.Range(1, random.Next(1, 24)).Select(ti => new Transaction { Amount = random.Next(1, 200)});
                        var xrefs = transactions.Select(t => new XrefTransactionDataFile { DataFile = file, Transaction = t });
                        // context.DataFiles.Add(file);
                        // context.Transactions.AddRange(transactions);
                        context.XrefTransactionDataFiles.AddRange(xrefs);
                        break;
                }
            }
            context.SaveChanges();
        }
    }

    [CollectionDefinition("INeedADatabase")]
    public class DatabaseFixtureCollection: ICollectionFixture<DatabaseFixture> { }
}