using System;

namespace AggregateSortingWithEntityFramework.Tests
{
    public class Transaction
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
    }

    public class DataFile
    {
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public string FileName { get; set; }
    }

    public class XrefTransactionDataFile
    {
        public Transaction Transaction { get; set; }
        public int TransactionId { get; set; }
        public DataFile DataFile { get; set; }
        public int DataFileId { get; set; }
    }
}