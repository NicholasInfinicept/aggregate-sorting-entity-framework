using System;
using System.Linq;
using Xunit;
using Xunit.Abstractions;

namespace AggregateSortingWithEntityFramework.Tests
{
    [Collection("INeedADatabase")]
    public class AggregateSortingTests
    {
        private readonly ITestOutputHelper _output;
        public AggregateSortingTests(ITestOutputHelper output) => _output = output;

        [Fact]
        public void AQueryThatOrdersFilesByTheSumOfTheirAssociatedTransactionsIsSane()
        {
            var context = new TestContext(DatabaseFixture.ConnectionString.ConnectionString);
            var result = context.XrefTransactionDataFiles
                            .Join(context.Transactions,
                                x => x.TransactionId,
                                t => t.Id,
                                (x, t) => new { x.DataFileId, t.Amount })
                            .GroupBy(x => x.DataFileId)
                            .Select(g => new { FileId = g.Key, Sum = g.Sum(g2 => g2.Amount) })
                            .OrderByDescending(x => x.Sum)
                            .ToList();

            foreach (var file in result)
            {
                _output.WriteLine($"File Id {file.FileId} has {file.Sum} in total $");
            }
        }

        [Fact]
        public void AQueryForFilesThatOrdersFilesByTheSumOfTheirAssociatedTransactionsIsSane()
        {
            var context = new TestContext(DatabaseFixture.ConnectionString.ConnectionString);
            var result = context.XrefTransactionDataFiles
                            .Join(context.Transactions,
                                x => x.TransactionId,
                                t => t.Id,
                                (x, t) => new { x.DataFileId, t.Amount })
                            .GroupBy(x => x.DataFileId)
                            .Select(g => new { FileId = g.Key, Sum = g.Sum(g2 => g2.Amount) })
                            .OrderByDescending(x => x.Sum)
                            .Join(context.DataFiles,
                                x => x.FileId,
                                d => d.Id,
                                (x, d) => d)
                            .ToList();

            foreach (var file in result)
            {
                _output.WriteLine($"File with Id {file.Id} and name {file.FileName} appears in this position in the list.");
            }
        }
    }
}
