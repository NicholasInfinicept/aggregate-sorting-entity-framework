using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace AggregateSortingWithEntityFramework.Tests
{
    public class TestContext : DbContext 
    {
        public TestContext(string connectionString) 
            : base(new DbContextOptionsBuilder<TestContext>().UseSqlServer(connectionString).Options)
        {
            Database.Migrate();
        }

        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<DataFile> DataFiles { get; set; }
        public DbSet<XrefTransactionDataFile> XrefTransactionDataFiles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<XrefTransactionDataFile>().HasKey(xref => new { xref.DataFileId, xref.TransactionId });
        }
    }

    public class DesignFactory : IDesignTimeDbContextFactory<TestContext>
    {
        public TestContext CreateDbContext(string[] args)
            => new TestContext(DatabaseFixture.ConnectionString.ConnectionString);
    }
}